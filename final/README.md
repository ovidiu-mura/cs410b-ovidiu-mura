# Final Project - Play Tic Tac Toe and win ether: Vyper Contract

+ Summary

I implemented a simple interface which loads information from the deployed 
Vyper contract and plays Tic Tac Toe game in JavaScript. The owner of the game sends 
1 ether to the winner of the game after the game is completed. If there is a 
draw, none of the players receive ether.

The application needs the following tools installed:

+ RPC server: It is a remote procedural server which uses inter-processing 
  communication technology and provides blockchain testing environment
+ Metamask: It is a bridge that allows to communicate between a distributed 
  network and the browser. It can be installed as plugin in Chrome

Install the application which has the following main components:

+ index.hml
  The web interface which loads Web3 JavaScript library to talk to the deployed 
  Vyper contract and the application; jQuery, and bootstrap included.
+ game.js
  Implementation of Tic Tac Toe game
+ game.vy
  Vyper smart game contract

Deploy Vyper contract:

+ Copy the contract’s ABI into the JS variable ‘contractAbi’, file: index.html
+ Copy the  the contract address into the JS variable ‘contractAddress’, file: index.html

Game Rules:
+ Player X identified with blockchain account
+ Player O identified with blockchain account
+ Game owner account balance greater than 1 ether (1000000000000000000 wei)
+ Player X or Player O make first move then the other player util there are three squares marked by the same player in row, column or diagonals
+ If there is a winner, the application automatically transfer 1 ether from the game owner account to the winner’s account
+ If there is a draw, no ether transferred
+ To play again refresh the page

Play:
+ Start Ganache rpc server
+ Load Player X’s address of blockchain account into web interface
+ Load Player O’s address of blockchain account into web interface
+ Login into Chrome metamask wallet with account balance greater than 1 ether (1000000000000000000 wei)
+ Play and win!

Vyper Code:

Map variables
+ bals map
+ addrs map

Methods
+ The constructor
+ Setters
+ Getters

JS code:

game.js file contains the following methods:
+ initialize_board
+ play
+ is_winner
+ select
+ transfer_funds

index.html file
+ Form
+ input and button
+ jQuery
+ Web3
+ HttpProvider: localhost:7545

transfer_funds method from game.js file
+ Transfer 1 ether from game owner to the winner’s account





+ url video presentation: https://media.pdx.edu/channel/FinalProjectsBlockchain/146174212


Tools and programming languages used: 

+ Vyper
+ Javascript, jQuery
+ html, css
+ Ganache RPC Server: https://www.trufflesuite.com/ganache 
+ IDE: Atom
+ Vyper REMIX IDE: http://remix.ethereum.org/
+ Vyper compiler: https://vyper.online/