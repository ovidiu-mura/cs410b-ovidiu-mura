var n = 3;
var EMPTY = "&nbsp;";
var boxes = [];
var turn = "X";
var score;
var moves;

function initialize_board() {
	/*
	It initializes the board game and sets the event for the click action
	*/
	var board = document.createElement('table');
	board.setAttribute("border", 1);
	board.setAttribute("cellspacing", 0);

	var identifier = 1;
	for (var i = 0; i < n; i++) {
		var row = document.createElement('tr');
		board.appendChild(row);
		for (var j = 0; j < n; j++) {
			var cell = document.createElement('td');
			cell.setAttribute('height', 50);
			cell.setAttribute('width', 50);
			cell.setAttribute('align', 'center');
			cell.setAttribute('valign', 'center');
			cell.classList.add('col' + j,'row' + i);
			if (i == j) {
				cell.classList.add('diag0');
			}
			if (j == n - i - 1) {
				cell.classList.add('diag1');
			}
			cell.identifier = identifier;
			cell.addEventListener("click", select);
			row.appendChild(cell);
			boxes.push(cell);
			identifier += identifier;
		}
	}
	document.getElementById("tictactoe").appendChild(board);
	play();
}

function play() {
	/*
	It plays the game; fills the board boxes with the corresponding mark or
	leave the boxes empty.
	*/
	score = {
		"X": 0,
		"O": 0
	};
	moves = 0;
	turn = "X";
	boxes.forEach(function (cell) {
		cell.innerHTML = EMPTY;
	});
}

function is_winner(c) {
	/*
	It checks if there is a winner.
	*/
	var classes = c.className.split(/\s+/);
	for (var i = 0; i < classes.length; i++) {
		var tc = '.' + classes[i];
		var it = [].filter.call(document.querySelectorAll('#tictactoe ' + tc), function(e){return RegExp(turn).test(e.textContent);});
		if (it.length == n) {
			it[0].style = "background-color:#49FF33";
			it[1].style = "background-color:#49FF33";
			it[2].style = "background-color:#49FF33";
			return true;
		}
	}
	return false;
}

function select() {
	/*
	It perform the action of the selected square on the games's board.
	If the game compelted and there is a winner, transfer 1 ether from
	the game owner to the winner.
	*/
	if (this.innerHTML !== EMPTY) {
		return;
	}
	this.innerHTML = turn;
	moves += 1;
	score[turn] += this.identifier;
	if (is_winner(this)) {
		document.getElementById('turn').style = "background-color:#49FF33";
		document.getElementById('turn').textContent = 'Winner: Player ' + turn;
		transfer_funds(turn);
	} else if (moves === n * n) {
		document.getElementById('turn').textContent = 'Draw';
	} else {
		turn = turn === "X" ? "O" : "X";
		document.getElementById('turn').textContent = 'Turn: Player ' + turn;
	}
}

function transfer_funds(player) {
	/*
	It transfers 1 ether from the game owner account to the winner player account.
	*/
	X_addr = web3.toHex(contract.get_x_a()).toString();
	O_addr = web3.toHex(contract.get_o_a()).toString();
	if (player === "X") {
		web3.eth.sendTransaction({ from: web3.eth.defaultAccount, to:X_addr, gas: 4000000, value:1000000000000000000 }, function (error, result) {
			if (!error) {
				console.log(result);
			} else {
				console.log(error);
			}
		});
		contract.set_owner();
		contract.set_x_a(X_addr);
	} else {
		web3.eth.sendTransaction({ from: web3.eth.defaultAccount, to:O_addr, gas: 4000000, value:1000000000000000000 }, function (error, result) {
			if (!error) {
				console.log(result);
			} else {
				console.log(error);
			}
		});
		contract.set_owner();
		contract.set_o_a(O_addr);
	}
}

initialize_board();
