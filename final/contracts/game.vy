# the balances of all the players and the owner accounts
bals: public(map(address, uint256(wei)))
# the addresses of the players and the owner accounts
addrs: public(map(int128, address))

@public
def __init__():
    # the constructor, initializes the owner address and balance
    self.addrs[0] = msg.sender
    self.bals[msg.sender] = msg.sender.balance

@public
def set_owner():
    # set the owner address and balance
    assert msg.sender.balance > 1000000000000000000
    self.addrs[0] = msg.sender
    self.bals[self.addrs[0]] = msg.sender.balance

@public
@payable
def set_bals(adr:address, bal: uint256(wei)) -> bool:
    # set the balance and the address for a given address player
    self.bals[adr] = bal
    return True

@public
@constant
def get_bal(adr: address) -> uint256(wei):
    # get the balance of a given player's account address
    return self.bals[adr]

@public
def set_x_a(x_addr: address) -> bool:
    # set the address and balance of the player X
    self.bals[x_addr] = x_addr.balance
    self.addrs[1] = x_addr
    return True

@public
def set_o_a(o_addr: address) -> bool:
    # set the address and balance of the player O
    self.bals[o_addr] = o_addr.balance
    self.addrs[2] = o_addr
    return True

@public
@constant
def get_x_a() -> address:
    # get the address of the player X
    return self.addrs[1]

@public
@constant
def get_o_a() -> address:
    # get the address of the player O
    return self.addrs[2]

@public
@constant
def get_balance_a() -> uint256(wei):
    # get the balance of the owner
    return self.bals[self.addrs[0]]

@public
def set_a(addr: address) -> bool:
    # set the address of the owner's account address
    self.addrs[0] = addr
    self.bals[self.addrs[0]] = addr.balance
    return True

@public
@constant
def get_a() -> address:
    # get the address of the owner's account address
    return self.addrs[0]
